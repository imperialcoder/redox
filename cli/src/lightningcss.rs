use std::{fs::File, io::Write, path::Path};
use watchexec::Watchexec;

use lightningcss::{
    bundler::{Bundler, FileProvider},
    stylesheet::{MinifyOptions, ParserFlags, ParserOptions, PrinterOptions},
};

fn export(fs: &FileProvider, index_location: &String, bundle_location: &String) {
    let mut bundler = Bundler::new(
        fs,
        None,
        ParserOptions {
            flags: ParserFlags::NESTING,
            ..Default::default()
        },
    );

    let mut stylesheet = bundler
        .bundle(Path::new(&index_location))
        .expect("'index_location' path does not exist");

    stylesheet
        .minify(MinifyOptions {
            ..Default::default()
        })
        .unwrap();

    let mut file =
        File::create(&bundle_location).expect("'bundle_location' is not a valid file path'");

    let res = stylesheet
        .to_css(PrinterOptions {
            minify: true,
            ..Default::default()
        })
        .unwrap();

    file.write(res.code.as_bytes())
        .expect("could not write res to file");
}

async fn watch() {
    let fs = FileProvider::new();
    let index_location = format!("{}/theme/styles/index.css", env!("CARGO_MANIFEST_DIR"));
    let bundle_location = format!("{}/theme/static/bundle.css", env!("CARGO_MANIFEST_DIR"));

    export(&fs, &index_location, &bundle_location);

    let wx = Watchexec::new(move |mut action| {
        if action.signals().next().is_some() {
            action.quit();
        } else {
            export(&fs, &index_location, &bundle_location);
        }
        action
    })
    .unwrap();

    wx.config.pathset(["./theme/styles"]);

    let _ = wx.main().await;
}

pub async fn main() {
    watch().await;
}
