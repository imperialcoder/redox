use core::panic;
use std::io::ErrorKind;
use std::os::unix::fs::symlink;

use std::path::PathBuf;
use std::{env, fs, process::Command};

pub struct Zola {
    cmd: Command,
    work_path: PathBuf,
    target_path: PathBuf,
}

impl Zola {
    pub fn new(site_name: String) -> Self {
        let work_path = match env::current_dir() {
            Ok(val) => val,
            Err(err) => panic!("{err}"),
        };
        let target_path = work_path.join("sites").join(&site_name);

        let mut cmd = Command::new("zola");
        cmd.arg("-r").arg(&target_path);

        return Self {
            cmd,
            work_path,
            target_path,
        };
    }

    pub async fn init(&mut self) {
        match fs::create_dir(&self.target_path) {
            Ok(_) => {}
            Err(err) => match err.kind() {
                ErrorKind::AlreadyExists => {}
                _ => panic!("{err}"),
            },
        };
        match self.cmd.arg("init").arg(&self.target_path).spawn() {
            Ok(mut cmd) => {
                cmd.wait().unwrap();
            }
            Err(err) => panic!("{err}"),
        };

        let org_theme_path = self.work_path.join("theme");
        let lnk_theme_path = self.target_path.join("themes/redox");
        match symlink(&org_theme_path, &lnk_theme_path) {
            Ok(_) => {}
            Err(err) => panic!("{err}"),
        }
    }

    pub async fn build(&mut self) {
        match self.cmd.arg("build").spawn() {
            Ok(mut cmd) => {
                cmd.wait().unwrap();
                cmd.kill().unwrap();
            }
            Err(err) => panic!("{err}"),
        };
    }

    pub async fn serve(&mut self) {
        match self.cmd.arg("serve").spawn() {
            Ok(mut cmd_result) => {
                cmd_result.wait().unwrap();
                cmd_result.kill().unwrap();
            }
            Err(err) => panic!("{err}"),
        }
    }
}
