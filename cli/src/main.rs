mod lightningcss;
mod zola;

use std::path::PathBuf;

use clap::{Parser, Subcommand};

use zola::Zola;

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    /// Sets a custom config file
    #[arg(short, long, value_name = "FILE")]
    config: Option<PathBuf>,

    /// Turn debugging information on
    #[arg(short, long, action = clap::ArgAction::Count)]
    debug: u8,

    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    /// create redox site
    Init { site: String },
    /// builds redox site
    Build { site: String },
    /// serves redox site
    Serve { site: String },
}

#[tokio::main]
async fn main() {
    let cli = Cli::parse();

    // You can see how many times a particular flag or argument occurred
    // Note, only flags can have multiple occurrences
    //match cli.debug {
    //    0 => println!("Debug mode is off"),
    //    1 => println!("Debug mode is kind of on"),
    //    2 => println!("Debug mode is on"),
    //    _ => println!("Don't be crazy"),
    //}

    // You can check for the existence of subcommands, and if found use their
    // matches just as you would the top level cmd
    match &cli.command {
        Some(Commands::Init { site }) => {
            println!("init {}", site);
            Zola::new(site.to_string()).init().await;
        }
        Some(Commands::Build { site }) => {
            println!("building {}", site);
            Zola::new(site.to_string()).build().await;
        }
        Some(Commands::Serve { site }) => {
            println!("serving {}", site);
            Zola::new(site.to_string()).serve().await;
            //tokio::join!(lightningcss::main(), zola::main(),);
        }
        _ => {}
    }
}
