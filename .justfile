sites_dir := 'sites'

# lists all recipes
default:
    just --list --unsorted

# lists serveable sites
list:
    tree -L 1 {{ sites_dir }}

# [build | run] - recipe for managing cli project 
cli $command *$options='':
    #!/usr/bin/env bash
    if [ $command == "build" ]; then
        cargo build -p redox $options
    elif [ $command == "run" ]; then
        cargo run -p redox $options
    fi


# [build | run] - recipe for managing the server project
server $command *$options='':
    #!/usr/bin/env bash
    if [ $command == "build" ]; then
        go build server $options
    elif [ $command == "run" ]; then
        go run server $options
    fi

# alias for running cli in dev mode
redox *$options='':
    @just cli run -q -- {{ options }}

# alias for building cli + server
build: (cli 'build') (server 'build')
