# redox

a multi-site hybrid JAMSTACK theme wrapper around zola utilizing
go, htmx, and lightningcss.

reduce, reuse, **redox!**

## index

- [dependencies](#dependencies)
- [quick_start](#quick_start)
- [getting_started](#getting_started)
  - [sites](#sites)
  - [server](#server)
  - [theme](#theme)
- [motivation](#motivation)
- [future_adds](#future_adds)
- [thanks_to](#thanks_to)

## dependencies

all dependencies must be installed for proper functionality

### for_the_websites

- \*[zola](https://www.getzola.org/)
- \*[just](https://github.com/casey/just)

### for_building/running_the_cli

- \*[rustup](https://rustup.rs/)
- \*[cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)

### for_building/running_the_server

- \*[go](https://go.dev/doc/install)

## quick_start

ready, set, GO!

```sh
# WIP
```

done!

## getting_started

at it's core, redox is all about reducing and reusing as much code as you can
in a zola as well as providing a backend and components written with go and htmx.
before jumping in, it's crucial to understand the directory structure of the project
and what each of them is for.

```sh
.
├── cli
│   └── src
├── server
│   └── src
├── sites
│   └── ...
└── theme
    ├── content
    ├── src
    ├── static
    └── templates
```

### cli

this directory contains all of our rust code for the cli! I like rust for the cli
and I cannot not lie :3

```sh
# building the cli via cargo
just cli build ...

# running the cli via cargo
just cli run ...

# alias for running redox (mostly for development testing)
just redox ...
```

### server

this directory contains all of our backend go code! this is just a standard go
project without anything too fancy.

```sh
# building the server
just server build

# running the server
just server run
```

### sites

the sites d#irectory is where we manage and store our generated zola sites. when
generated, we sym-link the ***theme*** directory to the zola's ***themes/redox***
directory for maximum code reuse across as many sites as we need.

```sh
# generating specified# zola site under 'clients' directory
just redox init example.com

# serving specified zola site to web-browser
just redox serve example.com

# building specified zola site to public directory
just redox build example.com

# checking specified zola site for valid code
just redox check example.com
```

### theme

this is the real star of the show, this is the zola theme for redox!

## motivation

I hated modern javascript but loved SSG's but felt like the bridge between creating
interactive experiences and the conviences of integrating an SSG where lacking, hence
this project!

I'm also aspiriing to be an entrepenurer and needed an easy + fast way to make
beautiful websites that work well with low maintenance and fun to hack on :3

## future_adds

I'd like to eventually make my own SSG in zig but more of a learning curve than I'm
willing to do ATM (full time job kinda holds me back).

## thanks_to

- [ThePrimeagen](https://www.youtube.com/@ThePrimeTimeagen)
  : for helping me out of my js-andy days

- [zola](https://www.getzola.org/)
  : for an amazing SSG that's **blazingly** fast

- [just](https://github.com/casey/just)
  : for creating a cool command runner
